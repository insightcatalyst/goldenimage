# packer puglin for AWS 
# https://www.packer.io/plugins/builders/amazon 
packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# which ami to use as the base and where to save it
source "amazon-ebs" "ubuntu" {
  region          = "eu-west-2"
  ami_name        = "ami-ubuntu-{{timestamp }}"
  instance_type   = "t2.micro"
  source_ami      = "ami-0e5f882be1900e43b"
  ssh_username    = "ubuntu"
  ami_users       = ["858272501714","611489710994"]
  ami_regions     = [
                      "us-east-1",
                      "eu-west-2"
                    ]

  tags = {
    Name = "ami-ubuntu-{{timestamp }}"
    CreatedBy = "packer"
  }

}

# what to install, configure and file to copy/execute
build {
  name = "ami-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "file" {
  source = "provisioner.sh"
  destination = "/tmp/provisioner.sh"
}

# https://www.packer.io/docs/provisioners/shell 
provisioner "shell" {
    inline = ["chmod a+x /tmp/provisioner.sh"]
  }

  provisioner "shell" {
    inline = ["/tmp/provisioner.sh"]
  }
}
